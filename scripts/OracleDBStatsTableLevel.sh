sqlplus << EOF
conn $1/$2
set pagesize 500
set linesize 1000
col table_name format a60
col last_analyzed format a40
SELECT TABLE_NAME,
       LAST_ANALYZED,
       NUM_ROWS,
       BLOCKS,
       AVG_ROW_LEN,
       SAMPLE_SIZE
  FROM user_tables;
EOF
