sqlplus / as sysdba <<EOF
col table_name for a30
col partition_name for a30
set lines 1000 pages 1000
SELECT table_name,
       partition_name
  FROM dba_tab_partitions where table_owner='$1'
 ORDER BY table_name, partition_name
;
EOF
