$DSE_HOME/bin/cqlsh -u $1 -p$2 $3 $4 <<EOF
select keyspace_name,
       table_name,
	   last_successful_validation,
	   last_unsuccessful_validation
  from system_distributed.nodesync_status;
EOF
