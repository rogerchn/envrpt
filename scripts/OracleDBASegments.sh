sqlplus / as sysdba <<EOF
set pagesize 500
set linesize 1000
col owner format a25
col segment_name format a40
select owner,segment_name,sum(bytes)/1024/1024/1024 as ""SIZE in GB"" from dba_segments 
where owner='$1' and segment_type='TABLE' group by owner,segment_name order by ""SIZE in GB"" desc;
EOF
