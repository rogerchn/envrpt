sqlplus / as sysdba << EOF
ALTER SESSION SET NLS_DATE_FORMAT = 'DD-MM-YYYY HH24:MI:SS';
set pagesize 1000
set linesize 1000 
col owner format a40
col last_analyzed format a40
SELECT owner,
       MIN(last_analyzed) as "LAST_ANALYZED"
  FROM dba_tables
 WHERE owner = '$1'
 GROUP BY owner
 ORDER BY 1
/
EOF
