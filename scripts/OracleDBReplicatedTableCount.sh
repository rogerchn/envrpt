sqlplus << EOF
conn $1/$2
set serveroutput on size unlimited
exec check_table_counts('$3');
EOF
