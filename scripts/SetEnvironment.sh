#!/bin/bash

echo "> ENVIRONMENT SETUP"

export ORACLE_HOME=/u01/app/oracle/product/12.2.0.1/db_1
export ORACLE_SID=DB12C
export GGS_HOME=/u01/oracle/product/goldengate
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$ORACLE_HOME/lib:$GGS_HOME
export PATH=$PATH:$ORACLE_HOME/bin:$GGS_HOME
export TMP_FOLDER=/tmp

echo "Oracle home from same script: $ORACLE_HOME"
