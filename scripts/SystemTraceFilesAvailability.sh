files=$(echo $1  | tr ";" "\n")
for file in $files
do
    set -- `echo $file | tr '=' ' '`
    fName=$1
    fLocn=$2
    if test -f $fLocn; then
        echo $fName:1:$fLocn
    else
        echo $fName:0:$fLocn
    fi
done
