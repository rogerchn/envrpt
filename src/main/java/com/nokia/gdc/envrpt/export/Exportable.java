/*
 * Copyright (c) $(year) Nokia Software. All rights reserved.
 * PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nokia.gdc.envrpt.export;

/**
 *
 * @author Roger Chacón <roger.chacon@nokia.com>
 */
public interface Exportable {

    public void export(String fileLocation);

    public String getFileExt();

}
