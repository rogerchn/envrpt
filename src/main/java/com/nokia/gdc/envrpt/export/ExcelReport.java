/*
 * Copyright (c) $(year) Nokia Software. All rights reserved.
 * PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nokia.gdc.envrpt.export;

import com.nokia.gdc.envrpt.domain.Parameter;
import com.nokia.gdc.envrpt.domain.Report;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

/**
 *
 * @author Roger Chacón <roger.chacon@nokia.com>
 */
public class ExcelReport implements Exportable {

    public ExcelReport(Report report) {
        this.report = report;
    }

    @Override
    public String getFileExt() {
        return fileExt;
    }

    /**
     * Exports the report data to an Excel spreadsheet.
     *
     * @param fileLocation The location where the document will be stored
     */
    @Override
    public void export(String fileLocation) {
        try ( InputStream inputStream = getClass().getClassLoader().getResourceAsStream("EnvRpt.xlsx")) {
            final Workbook workbook = WorkbookFactory.create(inputStream);
            final Sheet sheet = workbook.getSheetAt(0);
            final CellStyle categoryCellStyle = createCategoryStyle(workbook);
            final CellStyle segmentCellStyle = createSegmentStyle(workbook);
            report.getCategories().forEach(category -> {
                addSingleCellRow(sheet, categoryCellStyle, category.getName());
                addParamsRows(sheet, category.getParams());
                category.getSegments().forEach(segment -> {
                    addSingleCellRow(sheet, segmentCellStyle, segment.getName());
                    addParamsRows(sheet, segment.getParams());
                });
                addEmptyRow(sheet);
            });
            try ( OutputStream fileOut = new FileOutputStream(fileLocation)) {
                workbook.write(fileOut);
            }
        } catch (IOException e) {
            System.err.println("ERROR: " + e.getMessage() + "\n" + e);
        }
    }

    private final Report report;
    private final String fileExt = "xlsx";

    /**
     * Creates a new CellStyle in the specified Workbook.
     *
     * @param workbook The workbook
     * @return A CellStyle
     */
    private CellStyle createCategoryStyle(Workbook workbook) {
        final Font font = workbook.createFont();
        font.setBold(true);
        final CellStyle ctgCellStyle = workbook.createCellStyle();
        ctgCellStyle.setFont(font);
        ctgCellStyle.setAlignment(HorizontalAlignment.LEFT);
        return ctgCellStyle;
    }

    /**
     * Creates a new CellStyle in the specified Workbook.
     *
     * @param workbook The workbook
     * @return A CellStyle
     */
    private CellStyle createSegmentStyle(Workbook workbook) {
        final Font sgmFont = workbook.createFont();
        sgmFont.setItalic(true);
        final CellStyle sgmCellStyle = workbook.createCellStyle();
        sgmCellStyle.setFont(sgmFont);
        sgmCellStyle.setAlignment(HorizontalAlignment.LEFT);
        return sgmCellStyle;
    }

    /**
     * Adds a new Row containing a single Cell into the provided Sheet.
     *
     * @param sheet The sheet where the Row will be added.
     * @param cellStyle The style to be applied on the Cell.
     * @param value The value to be assigned to the Cell.
     */
    private void addSingleCellRow(Sheet sheet, CellStyle cellStyle, String value) {
        Row row = sheet.createRow(sheet.getLastRowNum() + 1);
        Cell cell = row.createCell(0);
        cell.setCellStyle(cellStyle);
        cell.setCellValue(value);
    }

    /**
     * Adds a new Cell into the provided Row using the Status position and
     * CellStyle.
     *
     * @param row The Row in which the new Cell will be added.
     * @param value The value to be assigned to the new Cell.
     */
    private void addStatusCell(Row row, int value) {
        final CellStyle cellStyle = row.getSheet().getWorkbook().createCellStyle();
        cellStyle.setAlignment(HorizontalAlignment.CENTER);
        Cell cell = row.createCell(0);
        cell.setCellStyle(cellStyle);
        cell.setCellValue(value);
    }

    /**
     * Adds a new Cell into the provided Row using the default CellStyle.
     *
     * Adds a new row using the default status CellStyle in the provided Sheet.
     *
     * @param row The Row in which the new cell will be added.
     * @param index The index within the Row where the new Cell will be added.
     * @param value The value to be assigned to the new Cell.
     */
    private void addDefaultCell(Row row, int index, String value) {
        final CellStyle cellStyle = row.getSheet().getWorkbook().createCellStyle();
        cellStyle.setAlignment(HorizontalAlignment.LEFT);
        final Cell cell = row.createCell(index);
        cell.setCellStyle(cellStyle);
        if (value.length() > 0 && value.trim().matches("^-?(\\d+|\\d+\\.|\\.\\d+|\\d+\\.\\d+)$")) {
            cell.setCellValue(Double.parseDouble(value));
        } else {
            cell.setCellValue(value);
        }
    }

    /**
     * Adds an empty row to the provided Sheet.
     *
     * @param sheet The Sheet where the empty Row will be added.
     */
    private void addEmptyRow(Sheet sheet) {
        Row row = sheet.createRow(sheet.getLastRowNum() + 1);
        Cell cell = row.createCell(0);
        cell.setCellValue("");
    }

    /**
     * Adds a Row for each Parameter from the List into the provided Sheet.
     *
     * @param sheet The Sheet where the rows will be added.
     * @param params A List of parameters to be added.
     */
    private void addParamsRows(Sheet sheet, List<Parameter> params) {
        params.forEach(param -> {
            Row row = sheet.createRow(sheet.getLastRowNum() + 1);
            addStatusCell(row, param.getStatus());
            addDefaultCell(row, 1, param.getName());
            addDefaultCell(row, 2, param.getCurrentValue());
            addDefaultCell(row, 3, param.getExpectedValue());
        });
    }

}
