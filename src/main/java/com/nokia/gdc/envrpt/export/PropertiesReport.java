/*
 * Copyright (c) $(year) Nokia Software. All rights reserved.
 * PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nokia.gdc.envrpt.export;

import com.nokia.gdc.envrpt.domain.Report;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;
import org.apache.commons.lang3.text.WordUtils;

/**
 *
 * @author Roger Chacón <roger.chacon@nokia.com>
 */
public class PropertiesReport implements Exportable {

    public PropertiesReport(Report report) {
        this.report = report;
    }

    @Override
    public String getFileExt() {
        return fileExt;
    }

    /**
     * Exports the report data to a Properties file.
     *
     * @param fileLocation The location where the document will be stored
     */
    @Override
    public void export(String fileLocation) {
        final Properties properties = new Properties();
        report.getCategories().forEach(category -> {
            String categoryName = WordUtils.capitalizeFully(category.getName()).replaceAll(" ", "");
            category.getParams().forEach(param -> {
                String paramName = WordUtils.capitalizeFully(param.getName()).replaceAll(" ", "");
                properties.setProperty(categoryName + "." + paramName + ".value", param.getCurrentValue());
                properties.setProperty(categoryName + "." + paramName + ".expected", param.getExpectedValue());
            });
            category.getSegments().forEach(segment -> {
                String segmentName = WordUtils.capitalizeFully(segment.getName()).replaceAll(" ", "");
                segment.getParams().forEach(param -> {
                    String paramName = WordUtils.capitalizeFully(param.getName()).replaceAll(" ", "");
                    properties.setProperty(categoryName + "." + segmentName + "." + paramName + ".value", param.getCurrentValue());
                    properties.setProperty(categoryName + "." + segmentName + "." + paramName + ".expected", param.getExpectedValue());
                });
            });
        });
        try {
            properties.store(new FileOutputStream(fileLocation), null);
        } catch (IOException e) {
            System.err.println("ERROR: " + e.getMessage() + "\n" + e);
        }
    }

    private final Report report;
    private final String fileExt = "properties";

}
