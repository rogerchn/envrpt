/*
 * Copyright (c) $(year) Nokia Software. All rights reserved.
 * PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nokia.gdc.envrpt.domain;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Roger Chacón <roger.chacon@nokia.com>
 */
public class Segment {

    public Segment(String name) {
        this.name = name;
        this.params = new ArrayList<>();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Parameter> getParams() {
        return params;
    }

    public void setParams(List<Parameter> params) {
        this.params = params;
    }

    private String name = "";
    private List<Parameter> params;

}
