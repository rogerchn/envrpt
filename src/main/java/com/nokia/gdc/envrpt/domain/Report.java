/**
 * Copyright (c) $(year) Nokia Software. All rights reserved.
 * PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 */
package com.nokia.gdc.envrpt.domain;

import java.util.ArrayList;
import java.util.List;
import org.apache.commons.lang3.StringUtils;

/**
 *
 * @author Roger Chacón <roger.chacon@nokia.com>
 */
public class Report {

    public Report() {
        this.categories = new ArrayList<>();
    }

    public List<Category> getCategories() {
        return categories;
    }

    public void setCategories(List<Category> categories) {
        this.categories = categories;
    }

    /**
     * Appends data to the current model. If the categories or segments already
     * exists entries are merged. Note that if a parameter exists within the
     * same category or segment it is NOT replaced, both entries are kept.
     *
     * @param newData The new data to be added to the data model.
     */
    public void append(Report newData) {
        newData.getCategories().forEach((newCat) -> {
            Category existingCat;
            if ((existingCat = getCategory(newCat)) != null) {
                existingCat.append(newCat);
            } else {
                categories.add(newCat);
            }
        });
    }

    /**
     * Indicates if a category already exists within this Report.
     *
     * @param categoryName The category name to look for.
     * @return true if the category exists, false otherwise.
     */
    public boolean hasCategory(String categoryName) {
        return categories.stream().anyMatch((category) -> (category.getName().equals(categoryName)));
    }

    /**
     * Indicates if a category already exists within this Report.
     *
     * @param category The category to look for.
     * @return true if the category exists, false if not.
     */
    public boolean hasCategory(Category category) {
        return hasCategory(category.getName());
    }

    /**
     * Gets a category by name from this Report.
     *
     * @param categoryName The category name to look for.
     * @return The Category if it exists, null otherwise.
     */
    public Category getCategory(String categoryName) {
        Category category = null;
        for (Category existing : categories) {
            if (existing.getName().equals(categoryName)) {
                category = existing;
            }
        }
        return category;
    }

    /**
     * Gets a category from this Report.
     *
     * @param category The category to look for.
     * @return The Category if it exists, null otherwise.
     */
    public Category getCategory(Category category) {
        return getCategory(category.getName());
    }

    /**
     * Adds new parameters to the specified category.
     *
     * @param categoryName The name of the category where the entries will be
     * added.
     * @param params A list of Parameters to be added.
     */
    public void addParams(String categoryName, List<Parameter> params) {
        addParams(categoryName, null, params);
    }

    /**
     * Adds new parameters to the specified category.
     *
     * @param categoryName The name of the category where the entries will be
     * added.
     * @param params A list of Parameters to be added.
     * @return The current Report.
     */
    public Report withParams(String categoryName, List<Parameter> params) {
        addParams(categoryName, params);
        return this;
    }

    /**
     * Adds new parameters to the specified category and segment.
     *
     * @param categoryName The name of the category where the entries will be
     * added.
     * @param segmentName The name of the segment within the given category.
     * @param params A list of Parameters to be added.
     */
    public void addParams(String categoryName, String segmentName, List<Parameter> params) {
        List paramList;
        Category category = this.getCategory(categoryName);
        if (category == null) {
            category = new Category(categoryName);
            categories.add(category);
        }
        if (StringUtils.isBlank(segmentName)) {
            paramList = category.getParams();
        } else {
            Segment segment = category.getSegment(segmentName);
            if (segment == null) {
                segment = new Segment(segmentName);
                category.getSegments().add(segment);
            }
            paramList = segment.getParams();
        }
        paramList.addAll(params);
    }

    /**
     * Adds new parameters to the specified category and segment.
     *
     * @param categoryName The name of the category where the entries will be
     * added.
     * @param segmentName The name of the segment within the given category.
     * @param params A list of Parameters to be added.
     * @return The current Report.
     */
    public Report withParams(String categoryName, String segmentName, List<Parameter> params) {
        addParams(categoryName, segmentName, params);
        return this;
    }

    /**
     * Adds a new parameter to the specified category.
     *
     * @param category The name of the category where the new parameter will be
     * added.
     * @param param The Parameter to be added.
     */
    public void addParam(String category, Parameter param) {
        List<Parameter> params = new ArrayList<>();
        params.add(param);
        addParams(category, params);
    }

    /**
     * Adds a new parameter to the specified category.
     *
     * @param category The name of the category where the new parameter will be
     * added.
     * @param param The Parameter to be added.
     * @return The current Report.
     */
    public Report withParam(String category, Parameter param) {
        addParam(category, param);
        return this;
    }

    /**
     * Adds a new parameter to the specified category.
     *
     * @param category The name of the category where the new parameter will be
     * added.
     * @param segment The name of the segment within the given category.
     * @param param The Parameter to be added.
     */
    public void addParam(String category, String segment, Parameter param) {
        List<Parameter> params = new ArrayList<>();
        params.add(param);
        addParams(category, segment, params);
    }

    /**
     * Adds a new parameter to the specified category.
     *
     * @param category The name of the category where the new parameter will be
     * added.
     * @param segment The name of the segment within the given category.
     * @param param The Parameter to be added.
     * @return The current Report.
     */
    public Report withParam(String category, String segment, Parameter param) {
        addParam(category, segment, param);
        return this;
    }

    private List<Category> categories;

}
