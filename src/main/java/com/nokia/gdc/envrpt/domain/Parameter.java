/*
 * Copyright (c) $(year) Nokia Software. All rights reserved.
 * PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nokia.gdc.envrpt.domain;

/**
 *
 * @author Roger Chacón <roger.chacon@nokia.com>
 */
public class Parameter {

    public static final int STATUS_INFO = 2;
    public static final int STATUS_GOOD = 1;
    public static final int STATUS_WARN = 0;
    public static final int STATUS_BAD = -1;

    public Parameter(String name) {
        this.name = name;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public void setStatusBad() {
        this.status = STATUS_BAD;
    }

    public void setStatusWarn() {
        this.status = STATUS_WARN;
    }

    public void setStatusGood() {
        this.status = STATUS_GOOD;
    }

    public void setStatusInfo() {
        this.status = STATUS_INFO;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCurrentValue() {
        return currentValue;
    }

    public void setCurrentValue(String currentValue) {
        this.currentValue = currentValue;
    }

    public String getExpectedValue() {
        return expectedValue;
    }

    public void setExpectedValue(String expectedValue) {
        this.expectedValue = expectedValue;
    }

    public Parameter withStatus(int status) {
        this.status = status;
        return this;
    }

    public Parameter withStatusBad() {
        setStatusBad();
        return this;
    }

    public Parameter withStatusWarn() {
        setStatusWarn();
        return this;
    }

    public Parameter withStatusGood() {
        setStatusGood();
        return this;
    }

    public Parameter withStatusInfo() {
        setStatusInfo();
        return this;
    }

    public Parameter withCurrentValue(String currentValue) {
        this.currentValue = currentValue;
        return this;
    }

    public Parameter withExpectedValue(String expectedValue) {
        this.expectedValue = expectedValue;
        return this;
    }

    private int status = 0;
    private String name;
    private String currentValue = "";
    private String expectedValue = "";

}
