/*
 * Copyright (c) $(year) Nokia Software. All rights reserved.
 * PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nokia.gdc.envrpt.domain;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Roger Chacón <roger.chacon@nokia.com>
 */
public class Category {

    public Category(String name) {
        this.name = name;
        params = new ArrayList<>();
        segments = new ArrayList<>();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Parameter> getParams() {
        return params;
    }

    public void setParams(List<Parameter> params) {
        this.params = params;
    }

    public List<Segment> getSegments() {
        return segments;
    }

    public void setSegments(List<Segment> segments) {
        this.segments = segments;
    }

    public void append(Category cat) {
        params.addAll(cat.getParams());
        cat.getSegments().forEach((newSegment) -> {
            Segment existingSegment;
            if ((existingSegment = getSegment(newSegment)) != null) {
                existingSegment.getParams().addAll(cat.getParams());
            } else {
                segments.add(newSegment);
            }
        });

    }

    /**
     * Indicates if a Segment already exists within this Category.
     *
     * @param segmentName The Segment name to look for.
     * @return true if the Segment exists, false otherwise.
     */
    public boolean hasSegment(String segmentName) {
        return segments.stream().anyMatch((segment) -> (segment.getName().equals(segmentName)));
    }

    /**
     * Indicates if a Segment already exists within this Category.
     *
     * @param segment The Segment to look for.
     * @return true if the Segment exists, false if not.
     */
    public boolean hasSegment(Segment segment) {
        return hasSegment(segment.getName());
    }

    /**
     * Gets a Segment by name from this Category.
     *
     * @param segmentName The segment name to look for.
     * @return The Segment if it exists, null otherwise.
     */
    public Segment getSegment(String segmentName) {
        Segment segment = null;
        for (Segment existing : segments) {
            if (existing.getName().equals(segmentName)) {
                segment = existing;
            }
        }
        return segment;
    }

    /**
     * Gets a Segment from this Category.
     *
     * @param segment The Segment to look for.
     * @return The Segment if it exists, null otherwise.
     */
    public Segment getSegment(Segment segment) {
        return getSegment(segment.getName());
    }

    private String name;
    private List<Parameter> params;
    private List<Segment> segments;

}
