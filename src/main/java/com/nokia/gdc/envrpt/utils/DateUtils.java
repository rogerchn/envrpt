/**
 * Copyright (c) $(year) Nokia Software. All rights reserved.
 * PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 */
package com.nokia.gdc.envrpt.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * A set of useful methods.
 *
 * @author Roger Chacón <roger.chacon@nokia.com>
 */
public class DateUtils {

    /**
     * Creates a Date from a given text in the specified format
     *
     * @param textDate The text representing a date
     * @param format The date/time pattern to use
     * @return A Date object
     */
    public static Date parseString(String textDate, String format) {
        Date date;
        try {
            date = new SimpleDateFormat(format).parse(textDate);
        } catch (ParseException e) {
            date = null;
        }
        return date;
    }

}
