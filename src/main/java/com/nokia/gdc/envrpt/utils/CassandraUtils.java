/*
 * Copyright (c) $(year) Nokia Software. All rights reserved.
 * PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nokia.gdc.envrpt.utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Roger Chacón <roger.chacon@nokia.com>
 */
public class CassandraUtils {

    /**
     * Parses a Cassandra query response.
     *
     * @param textResult The plain text response
     * @return A list of maps with the values from the response
     */
    public static List<Map<String, String>> parseQueryResult(String textResult) {
        List<Map<String, String>> result = new ArrayList<>();
        String[] lines = textResult.trim().split("\n");
        if (lines.length > 2) {
            // First line contains the column names
            String columnNames = lines[0];
            // Second line describes the column sizes
            String[] descriptors = lines[1].split("\\+");
            List<Map<String, String>> columns = new ArrayList<>();
            int pointer = 0;
            for (String descriptor : descriptors) {
                Integer startPos = pointer;
                Integer endPos = pointer + descriptor.length();
                String name = columnNames.length() > endPos ? columnNames.substring(startPos, endPos).trim() : columnNames.substring(startPos).trim();
                Map<String, String> column = new HashMap<>();
                column.put("name", name);
                column.put("startPos", startPos.toString());
                column.put("endPos", endPos.toString());
                columns.add(column);
                pointer = endPos + 1;
            }
            // Parse the rest of the lines
            for (int i = 2; i < lines.length; i++) {
                String line = lines[i];
                if (line.length() > 0 && !line.endsWith(" rows)")) {
                    Map<String, String> entry = new HashMap<>();
                    for (Map<String, String> column : columns) {
                        String name = column.get("name");
                        int startPos = Integer.parseInt(column.get("startPos"));
                        int endPos = Integer.parseInt(column.get("endPos"));
                        String value = line.length() > endPos ? line.substring(startPos, endPos).trim() : line.substring(startPos).trim();
                        entry.put(name, value);
                    }
                    result.add(entry);
                }
            }
        }
        return result;
    }

    /**
     * Validates Nodetool response.
     *
     * @param textResult The plain text response
     * @return false if Nodetool is not configured, true otherwise.
     */
    public static boolean isNodetoolConfigured(String textResult) {
        final String[] knownErrors = {
            "Cannot determine DSE_CONF."
        };
        for (String knownError : knownErrors) {
            if (knownError.equals(textResult.trim())) {
                return false;
            }
        }
        return true;
    }

}
