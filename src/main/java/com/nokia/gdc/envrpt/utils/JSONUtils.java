/*
 * Copyright (c) $(year) Nokia Software. All rights reserved.
 * PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nokia.gdc.envrpt.utils;

import java.util.Iterator;
import org.json.JSONObject;

/**
 *
 * @author Roger Chacón <roger.chacon@nokia.com>
 */
public class JSONUtils {

    /**
     * Merges the contents of two JSONObjects into a new one. Duplicated entries
     * found in obj2 will overwrite those on obj1.
     *
     * @param obj1 The first JSONObject
     * @param obj2 The second JSONObject
     * @return A new JSONObject containing the items from objects 1 and 2
     */
    public static JSONObject mergeJSONObjects(JSONObject obj1, JSONObject obj2) {
        JSONObject newObject = new JSONObject();
        JSONObject[] jsonCollections = {obj1, obj2};
        for (JSONObject collection : jsonCollections) {
            Iterator<String> keys = collection.keys();
            while (keys.hasNext()) {
                String key = keys.next();
                newObject.put(key, collection.get(key));
            }
        }
        return newObject;
    }

}
