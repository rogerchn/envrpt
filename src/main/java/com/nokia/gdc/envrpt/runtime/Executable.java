/*
 * Copyright (c) $(year) Nokia Software. All rights reserved.
 * PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nokia.gdc.envrpt.runtime;

/**
 *
 * @author Roger Chacón <roger.chacon@nokia.com>
 */
public interface Executable {

    public String execute(String cmd) throws Exception;

    public void setIgnoreErrors(boolean ignore);

    public boolean isSuccess();

}
