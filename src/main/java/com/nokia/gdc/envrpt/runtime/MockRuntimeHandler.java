/**
 * Copyright (c) $(year) Nokia Software. All rights reserved.
 * PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 */
package com.nokia.gdc.envrpt.runtime;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Roger Chacón <roger.chacon@nokia.com>
 */
public class MockRuntimeHandler implements Executable {

    public MockRuntimeHandler() {
        super();
        commands = new HashMap<>();
        commands.put("OracleDatabaseVersion", "scripts/OracleDBVersion.sh");
        commands.put("OracleGoldenGateVersion", "scripts/OracleGoldenGateVersion.sh");
        commands.put("OraclePatchLevels", "scripts/OraclePatchLevels.sh");
    }

    @Override
    public String execute(String command) throws IOException {
        String output = null;
        String cmdName = null;
        successful = false;
        for (String key : commands.keySet()) {
            String value = commands.get(key);
            if (command.contains(value)) {
                successful = true;
                cmdName = key;
                break;
            }
        }
        if (successful) {
            output = new String(Files.readAllBytes(Paths.get("C:\\Users\\rochacon\\OneDrive - Nokia\\Projects\\Verizon\\LW001\\EnvRpt\\mock\\" + cmdName + ".txt")), Charset.defaultCharset());
        }
        return output;
    }

    @Override
    public void setIgnoreErrors(boolean ignoreErrors) {
    }

    @Override
    public boolean isSuccess() {
        return successful;
    }

    private boolean successful;
    private final Map<String, String> commands;
}
