/**
 * Copyright (c) $(year) Nokia Software. All rights reserved.
 * PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 */
package com.nokia.gdc.envrpt.runtime;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 *
 * @author Roger Chacón <roger.chacon@nokia.com>
 */
public class RuntimeHandler implements Executable {

    public RuntimeHandler(String[] envp) {
        super();
        this.envp = envp;
        this.ignoreErrors = false;
    }

    @Override
    public String execute(String command) throws IOException, InterruptedException {
        Process process = Runtime.getRuntime().exec(command, envp);
        InputStreamReader inputStreamReader;
        StringBuilder builder = new StringBuilder();
        exitValue = process.waitFor();
        if (exitValue == 0 || ignoreErrors) {
            inputStreamReader = new InputStreamReader(process.getInputStream());
        } else {
            builder.append("Exit with code ").append(exitValue).append("\n");
            inputStreamReader = new InputStreamReader(process.getErrorStream());
        }
        BufferedReader reader = new BufferedReader(inputStreamReader);
        String line;
        int lineCount = 0;
        while ((line = reader.readLine()) != null) {
            if (lineCount++ > 0) {
                builder.append("\n");
            }
            builder.append(line);
        }
        return builder.toString();
    }

    @Override
    public void setIgnoreErrors(boolean ignoreErrors) {
        this.ignoreErrors = ignoreErrors;
    }

    @Override
    public boolean isSuccess() {
        return exitValue == 0;
    }

    private final String[] envp;
    private boolean ignoreErrors;
    private Integer exitValue = null;

}
