/**
 * Copyright (c) $(year) Nokia Software. All rights reserved.
 * PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 */
package com.nokia.gdc.envrpt;

import com.nokia.gdc.envrpt.runtime.RuntimeHandler;
import java.io.IOException;
import com.nokia.gdc.envrpt.check.Checkable;
import com.nokia.gdc.envrpt.domain.Report;
import com.nokia.gdc.envrpt.export.ExcelReport;
import com.nokia.gdc.envrpt.export.Exportable;
import com.nokia.gdc.envrpt.export.PropertiesReport;
import com.nokia.gdc.envrpt.runtime.Executable;
import com.nokia.gdc.envrpt.runtime.MockRuntimeHandler;
import com.nokia.gdc.envrpt.utils.JSONUtils;
import java.io.File;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author Roger Chacón <roger.chacon@nokia.com>
 */
public class Main {

    public static void main(String[] args) {

        System.out.println("EnvRpt process started!!!");

        // Load execution settings
        String profile = "Default";
        if (args.length > 0) {
            profile = args[0];
        }
        System.out.println("Using profile " + profile);

        // Set the output format handler
        Report report = new Report();
        String outputFormat = "Excel";
        if (args.length > 1) {
            outputFormat = args[1];
        }
        Exportable formatHandler;
        switch (outputFormat) {
            case "Properties":
                formatHandler = new PropertiesReport(report);
                break;
            case "Excel":
            default:
                formatHandler = new ExcelReport(report);
        }
        System.out.println("Output format: " + outputFormat);

        try {
            byte[] encoded = Files.readAllBytes(Paths.get("profiles/" + profile + ".json"));
            JSONObject settings = new JSONObject(new String(encoded, Charset.defaultCharset()));

            // Load environment properties
            String[] envp = new String[]{};
            if (settings.has("env-props")) {
                JSONArray envProps = settings.getJSONArray("env-props");
                envp = new String[envProps.length()];
                System.out.println("Environment properties:");
                for (int i = 0; i < envProps.length(); i++) {
                    envp[i] = envProps.getString(i);
                    System.out.println("  " + envp[i]);
                }
            } else {
                System.out.println("No environment properties found.");
            }

            // Set the system handler
            String rtHandler = "Runtime";
            if (args.length > 2) {
                rtHandler = args[2];
            }
            Executable executor;
            switch (rtHandler) {
                case "Mock":
                    executor = new MockRuntimeHandler();
                    break;
                case "Runtime":
                default:
                    executor = new RuntimeHandler(envp);
            }
            System.out.println("Runtime Handler: " + rtHandler);

            // Execute profile checks
            if (settings.has("checks")) {
                JSONArray checks = settings.getJSONArray("checks");
                if (checks.length() > 0) {
                    System.out.println("Running " + checks.length() + " checks.");
                    for (int i = 0; i < checks.length(); i++) {
                        JSONObject check = checks.getJSONObject(i);
                        if (check.getBoolean("enabled")) {
                            String checkName = check.getString("name");
                            System.out.println("\n<< " + checkName + " >>");
                            Class clazz = Class.forName("com.nokia.gdc.envrpt.check." + checkName);
                            Checkable checkable = (Checkable) clazz.getConstructor().newInstance();
                            try {
                                checkable.setContext(JSONUtils.mergeJSONObjects(settings.getJSONObject("context"), check.getJSONObject("context")));
                                String command = checkable.getCommand();
                                System.out.println("Command:\n" + command);
                                executor.setIgnoreErrors(checkable.isIgnoreErrors());
                                String output = executor.execute(command);
                                System.out.println("Result:\n" + output);
                                // Add results to report
                                Report result;
                                if (executor.isSuccess()) {
                                    result = checkable.parseResults(output);
                                } else {
                                    result = checkable.getErrorResult();
                                }
                                report.append(result);
                            } catch (IOException e) {
                                report.append(checkable.getErrorResult());
                                System.err.println("Error:\n" + e.getMessage() + "\n" + e);
                            }
                        }
                    }
                    // System.out.println("\n\n\n\n\nReport:\n" + report.toString());
                    String outputFolder = "/var/tmp";
                    if (settings.has("output-folder")) {
                        outputFolder = settings.getString("output-folder");
                    }
                    String reportFile = "EnvRpt_" + new SimpleDateFormat("yyyyMMdd-HHmmss").format(new Date()) + "." + formatHandler.getFileExt();
                    System.out.println("\nExporting report to " + reportFile + " ...");
                    formatHandler.export(outputFolder + File.separator + reportFile);
                    System.out.println("OK!");
                } else {
                    System.out.println("No checks configured.");
                }
            } else {
                System.out.println("No checks entry found.");
            }

            System.out.println("\nEnvRpt process completed!");
        } catch (Exception e) {
            System.err.println("Error:\n" + e.getMessage() + "\n" + e);
        }
    }

}
