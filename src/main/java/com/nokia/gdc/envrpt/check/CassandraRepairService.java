/**
 * Copyright (c) $(year) Nokia Software. All rights reserved.
 * PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 */
package com.nokia.gdc.envrpt.check;

import com.nokia.gdc.envrpt.domain.Report;
import com.nokia.gdc.envrpt.domain.Parameter;

/**
 *
 * @author Roger Chacón <roger.chacon@nokia.com>
 */
public class CassandraRepairService extends AbstractCheck {

    public CassandraRepairService() {
        catName = "Repair Service";
        paramNames = new String[]{paramStatus, paramTtc};
    }

    @Override
    public String getCommand() {
        return "scripts/CassandraRepairService.sh";
    }

    @Override
    public Report parseResults(String response) {

        // Parse status
        String expectedStatus = context.getString("status");
        int pos = response.indexOf("\"status\": \"");
        String status = response.substring(pos + 11).split("\n")[0].trim();
        Parameter param = new Parameter(paramStatus)
                .withCurrentValue(status)
                .withExpectedValue(expectedStatus);
        if (status.equalsIgnoreCase(expectedStatus)) {
            param.setStatusGood();
        } else {
            param.setStatusBad();
        }
        params.add(param);

        // Parse time to completion
        String expectedTtc = context.getString("timeToCompletion");
        pos = response.indexOf("\"time_to_completion\":");
        String ttc = response.substring(pos + 21).split(",")[0].trim();
        param = new Parameter(paramTtc)
                .withCurrentValue(ttc)
                .withExpectedValue(expectedTtc);
        if (ttc.equalsIgnoreCase(expectedTtc)) {
            param.setStatusGood();
        } else {
            param.setStatusBad();
        }
        params.add(param);

        return getDefaultReport();
    }

    private final String paramStatus = "Status";
    private final String paramTtc = "Time to completion";

}
