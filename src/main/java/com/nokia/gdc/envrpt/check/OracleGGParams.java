/*
 * Copyright (c) $(year) Nokia Software. All rights reserved.
 * PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nokia.gdc.envrpt.check;

import com.nokia.gdc.envrpt.domain.Report;

/**
 *
 * @author Roger Chacón <roger.chacon@nokia.com>
 */
public class OracleGGParams extends AbstractCheck {

    public OracleGGParams() {
        catName = "GoldenGate Parameters";
        paramNames = new String[]{
            "mgr", "excom", "exclog", "exdjr", "exdjrl", "exdjrd", "dpcom", "dpclog",
            "dpdjr", "dpdjrl", "dpdjrd", "rpcom", "rpclog", "rpdjr", "rpdjrl", "rpdjrd"
        };
    }

    @Override
    public String getCommand() {
        return "scripts/OracleGoldenGateParams.sh";
    }

    @Override
    public Report parseResults(String response) {
        return null;
    }

}
