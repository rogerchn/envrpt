/*
 * Copyright (c) $(year) Nokia Software. All rights reserved.
 * PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nokia.gdc.envrpt.check;

import com.nokia.gdc.envrpt.domain.Report;
import com.nokia.gdc.envrpt.domain.Parameter;
import com.nokia.gdc.envrpt.utils.CassandraUtils;
import org.apache.commons.lang3.StringUtils;

/**
 *
 * @author Roger Chacón <roger.chacon@nokia.com>
 */
public class CassandraNodesStatus extends AbstractCheck {

    public CassandraNodesStatus() {
        catName = "Cassandra Nodes";
        paramNames = new String[]{"Nodes"};
    }

    @Override
    public String getCommand() {
        String ntUser = context.getString("nodetool.user");
        String ntPass = context.getString("nodetool.pass");
        String keyspace = context.optString("keyspace");
        return "scripts/CassandraNodesStatus.sh " + ntUser + " " + ntPass + " " + keyspace;
    }

    @Override
    public Report parseResults(String response) {
        if (CassandraUtils.isNodetoolConfigured(response)) {
            String[] lines = response.split("\n");
            for (int i = 5; i < lines.length; i++) {
                String line = lines[i];
                if (StringUtils.isEmpty(line)) {
                    break;
                } else {
                    while (line.contains("  ")) {
                        line = line.replace("  ", " ");
                    }
                    String[] chunks = line.split(" ");
                    String pName = chunks[1];
                    String pValue = chunks[0];
                    final String expectedValue = "UN";
                    Parameter param = new Parameter(pName)
                            .withCurrentValue(pValue)
                            .withExpectedValue(expectedValue);
                    if (expectedValue.equals(pValue)) {
                        param.setStatusGood();
                    } else {
                        param.setStatusBad();
                    }
                    params.add(param);
                }
            }
        } else {
            Parameter param = new Parameter(response);
            param.setStatusWarn();
            params.add(param);
        }
        sgmName = context.optString("keyspace");
        return getDefaultReport();
    }

}
