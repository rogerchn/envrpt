/*
 * Copyright (c) $(year) Nokia Software. All rights reserved.
 * PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nokia.gdc.envrpt.check;

import com.nokia.gdc.envrpt.domain.Category;
import com.nokia.gdc.envrpt.domain.Parameter;
import com.nokia.gdc.envrpt.domain.Report;
import com.nokia.gdc.envrpt.domain.Segment;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Roger Chacón <roger.chacon@nokia.com>
 */
public class OraclePatchLevels extends AbstractCheck {

    public OraclePatchLevels() {
        catName = "Oracle Interim Patch Installer";
        paramNames = segmentNames;
    }

    @Override
    public String getCommand() {
        return "scripts/OraclePatchLevels.sh";
    }

    @Override
    public Report parseResults(String response) {
        // TODO: This entire method must be refactorized and use separated calls to patch command
        String[] lines = response.split("\n");
        final String startTag = "Oracle Interim Patch Installer version";
        final String endTag = "OPatch succeeded.";
        final String oPatchVersionTag = "OPatch version";
        final String oUIVersionTag = "OUI version";
        int segmentCount = 0;
        List<Segment> segments = new ArrayList<>();
        List<Parameter> paramList = new ArrayList<>();
        for (String line : lines) {
            if (line.trim().length() > 0) {
                if (line.startsWith(startTag)) {
                    paramList = new ArrayList<>();
                } else if (line.startsWith(endTag)) {
                    Segment segment = new Segment(segmentNames[segmentCount++]);
                    segment.getParams().addAll(paramList);
                    segments.add(segment);
                } else {
                    if (line.contains(":")) {
                        String[] chunks = line.split(":");
                        String paramName = chunks[0].trim();
                        if ((paramName.equals(oPatchVersionTag) || paramName.equals(oUIVersionTag))) {
                            String currentValue = chunks[1].trim();
                            String expectedValue = context.optString(paramName);
                            Parameter param = new Parameter(paramName)
                                    .withCurrentValue(currentValue)
                                    .withExpectedValue(expectedValue);
                            if (expectedValue.isEmpty()) {
                                param.setStatusInfo();
                            } else {
                                if (expectedValue.equals(currentValue)) {
                                    param.setStatusGood();
                                } else {
                                    param.setStatusBad();
                                }
                            }
                            paramList.add(param);
                        }
                    }
                }

            }
        }
        Category category = new Category(catName);
        category.getSegments().addAll(segments);
        Report report = new Report();
        report.getCategories().add(category);
        return report;
    }

    private final String[] segmentNames = {"Database", "GoldenGate"};

}
