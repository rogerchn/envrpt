/*
 * Copyright (c) $(year) Nokia Software. All rights reserved.
 * PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nokia.gdc.envrpt.check;

import com.nokia.gdc.envrpt.domain.Report;
import com.nokia.gdc.envrpt.domain.Parameter;
import com.nokia.gdc.envrpt.utils.CassandraUtils;

/**
 *
 * @author Roger Chacón <roger.chacon@nokia.com>
 */
public class CassandraNodeSlowLog extends AbstractCheck {

    public CassandraNodeSlowLog() {
        catName = "Node Slow Log";
        paramNames = new String[]{"Commands"};
    }

    @Override
    public String getCommand() {
        String cqlUser = context.getString("cqlsh.user");
        String cqlPass = context.getString("cqlsh.pass");
        String cqlHost = context.getString("cqlsh.host");
        String cqlPort = context.getString("cqlsh.port");
        return "scripts/CassandraNodeSlowLog.sh " + cqlUser + " " + cqlPass + " " + cqlHost + " " + cqlPort;
    }

    @Override
    public Report parseResults(String response) {
        CassandraUtils.parseQueryResult(response).forEach(entry -> {
            Parameter param = new Parameter(entry.get("node_ip") + " => " + entry.get("duration"))
                    .withCurrentValue(entry.get("commands"))
                    .withStatusBad();
            params.add(param);
        });
        return getDefaultReport();
    }

}
