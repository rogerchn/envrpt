/**
 * Copyright (c) $(year) Nokia Software. All rights reserved.
 * PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 */
package com.nokia.gdc.envrpt.check;

import com.nokia.gdc.envrpt.domain.Report;
import com.nokia.gdc.envrpt.domain.Parameter;
import org.apache.commons.lang3.StringUtils;

/**
 *
 * @author Roger Chacón <roger.chacon@nokia.com>
 */
public class SystemConfigFile extends AbstractCheck {

    @Override
    public String getCommand() {
        String file = context.getString("file");
        return "scripts/SystemCatFile.sh " + file;
    }

    @Override
    public Report parseResults(String response) {
        catName = context.getString("category");
        sgmName = context.getString("file");
        paramNames = new String[]{"File contents"};
        final String comment = "#";
        String[] lines = response.split("\n");
        for (String line : lines) {
            if (!line.isEmpty() && !line.startsWith(comment)) {
                final int pos = line.indexOf("=");
                String pName, pValue;
                if (pos >= 0) {
                    pName = line.substring(0, pos).trim();
                    if (line.length() > pos + 1) {
                        pValue = line.substring(pos + 1).trim();
                    } else {
                        pValue = StringUtils.EMPTY;
                    }
                } else {
                    pName = line.trim();
                    pValue = StringUtils.EMPTY;
                }
                String expected = context.optString(pName);
                Parameter param = new Parameter(pName)
                        .withCurrentValue(pValue)
                        .withExpectedValue(expected);
                if (expected.isEmpty()) {
                    param.setStatusInfo();
                } else {
                    if (expected.equals(pValue)) {
                        param.setStatusGood();
                    } else {
                        param.setStatusBad();
                    }
                }
                params.add(param);
            }
        }
        return getDefaultReport();
    }

}
