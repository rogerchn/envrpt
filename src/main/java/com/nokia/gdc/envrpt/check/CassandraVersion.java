/*
 * Copyright (c) $(year) Nokia Software. All rights reserved.
 * PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nokia.gdc.envrpt.check;

import com.nokia.gdc.envrpt.domain.Report;
import com.nokia.gdc.envrpt.domain.Parameter;

/**
 *
 * @author Roger Chacón <roger.chacon@nokia.com>
 */
public class CassandraVersion extends AbstractCheck {

    public CassandraVersion() {
        catName = "Cassandra Version";
        sgmName = "Cassandra";
        paramNames = new String[]{sgmName};
    }

    @Override
    public String getCommand() {
        String cqlUser = context.getString("cqlsh.user");
        String cqlPass = context.getString("cqlsh.pass");
        String cqlHost = context.getString("cqlsh.host");
        String cqlPort = context.getString("cqlsh.port");
        return "scripts/CassandraVersion.sh " + cqlUser + " " + cqlPass + " " + cqlHost + " " + cqlPort;
    }

    @Override
    public Report parseResults(String response) {
        String[] entries = response.substring(1, response.length() - 1).split("\\|");
        final String[] pNames = {"cqlsh", "DSE", "CQL spec", "DSE protocol"};
        for (String pName : pNames) {
            String pValue = null;
            for (String entry : entries) {
                entry = entry.trim();
                if (entry.toUpperCase().startsWith(pName.toUpperCase())) {
                    pValue = entry.substring(pName.length()).trim();
                    break;
                }
            }
            String expectedValue = context.optString(pName);
            Parameter param = new Parameter(pName)
                    .withCurrentValue(pValue != null ? pValue : "N/A")
                    .withExpectedValue(expectedValue);
            if (expectedValue.length() > 0) {
                if (expectedValue.equalsIgnoreCase(pValue)) {
                    param.setStatusGood();
                } else {
                    param.setStatusBad();
                }
            } else {
                param.setStatusInfo();
            }
            params.add(param);
        }
        return getDefaultReport();
    }

}
