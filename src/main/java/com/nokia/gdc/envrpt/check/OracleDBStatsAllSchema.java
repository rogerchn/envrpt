/*
 * Copyright (c) $(year) Nokia Software. All rights reserved.
 * PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nokia.gdc.envrpt.check;

import com.nokia.gdc.envrpt.domain.Report;
import com.nokia.gdc.envrpt.utils.DateUtils;
import com.nokia.gdc.envrpt.domain.Parameter;
import com.nokia.gdc.envrpt.utils.OracleUtils;
import java.util.Date;

/**
 *
 * @author Roger Chacón <roger.chacon@nokia.com>
 */
public class OracleDBStatsAllSchema extends AbstractCheck {

    public OracleDBStatsAllSchema() {
        catName = "Check Stats - All Schema";
        paramNames = new String[]{"All Schema"};
    }

    @Override
    public String getCommand() {
        return "scripts/OracleDBStatsAllSchema.sh";
    }

    @Override
    public Report parseResults(String response) {
        final String ctxLastAnalyzed = context.getString("lastAnalyzed");
        final String dtPattern = "dd/MM/yyyy HH:mm:ss";
        final Date minLastAnalyzed = DateUtils.parseString(ctxLastAnalyzed, dtPattern);
        OracleUtils.parseQueryResult(response).forEach(entry -> {
            final String owner = entry.get("OWNER");
            final String lastAnalyzed = entry.get("LAST_ANALYZED");
            final Date lastAnalyzedDt = DateUtils.parseString(lastAnalyzed, dtPattern);
            Parameter table = new Parameter(owner)
                    .withCurrentValue(lastAnalyzed)
                    .withExpectedValue(ctxLastAnalyzed);
            if (lastAnalyzedDt.before(minLastAnalyzed)) {
                table.setStatusBad();
            } else {
                table.setStatusGood();
            }
            params.add(table);
        });
        return getDefaultReport();
    }

}
