/**
 * Copyright (c) $(year) Nokia Software. All rights reserved.
 * PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 */
package com.nokia.gdc.envrpt.check;

import com.nokia.gdc.envrpt.domain.Report;
import com.nokia.gdc.envrpt.domain.Parameter;
import org.json.JSONArray;

/**
 *
 * @author Roger Chacón <roger.chacon@nokia.com>
 */
public class SystemTraceFilesAvailability extends AbstractCheck {

    @Override
    public String getCommand() {
        JSONArray files = context.getJSONArray("files");
        StringBuilder fileList = new StringBuilder();
        int fileCount = files.length();
        fileNames = new String[fileCount];
        for (int i = 0; i < fileCount; i++) {
            String file = files.getString(i);
            fileNames[i] = file.split("=")[0];
            fileList.append(file).append(";");
        }
        return "scripts/SystemTraceFilesAvailability.sh \"" + fileList.toString() + "\"";
    }

    @Override
    public Report parseResults(String response) {
        catName = "Files Availablity";
        paramNames = fileNames;
        String[] lines = response.split("\n");
        for (String line : lines) {
            String[] chunks = line.split(":");
            String fileName = chunks[0];
            String fileStatus = chunks[1];
            String fileLocation = chunks[2];
            Parameter param = new Parameter(fileName)
                    .withCurrentValue(fileLocation);
            if (fileStatus.equals("1")) {
                param.setStatusGood();
            } else {
                param.setStatusBad();
            }
            params.add(param);
        }
        return getDefaultReport();
    }

    private String[] fileNames;

}
