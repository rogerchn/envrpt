/**
 * Copyright (c) $(year) Nokia Software. All rights reserved.
 * PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 */
package com.nokia.gdc.envrpt.check;

import com.nokia.gdc.envrpt.domain.Report;
import com.nokia.gdc.envrpt.domain.Parameter;

/**
 *
 * @author Roger Chacón <roger.chacon@nokia.com>
 */
public class CassandraResourceLimitFile extends AbstractCheck {

    @Override
    public String getCommand() {
        String file = context.getString("file");
        return "scripts/SystemCatFile.sh " + file;
    }

    @Override
    public Report parseResults(String response) {
        catName = context.getString("category");
        sgmName = context.getString("file");
        paramNames = new String[]{sgmName};
        final String comment = "#";
        final String sep = " ";
        String[] lines = response.split("\n");
        for (String line : lines) {
            if (!line.isEmpty() && !line.startsWith(comment)) {
                String cleanLine = line.trim().replaceAll("\\s+", " ");
                String[] chunks = cleanLine.split(" ");
                String pName = chunks[0] + sep + chunks[1] + sep + chunks[2];
                String pValue = chunks[3];
                String expected = context.optString(pName);
                Parameter param = new Parameter(pName)
                        .withCurrentValue(pValue)
                        .withExpectedValue(expected);
                if (expected.isEmpty()) {
                    param.setStatusInfo();
                } else {
                    if (expected.equals(pValue)) {
                        param.setStatusGood();
                    } else {
                        param.setStatusBad();
                    }
                }
                params.add(param);
            }
        }
        return getDefaultReport();
    }

}
