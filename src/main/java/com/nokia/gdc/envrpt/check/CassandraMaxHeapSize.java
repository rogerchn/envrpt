/**
 * Copyright (c) $(year) Nokia Software. All rights reserved.
 * PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 */
package com.nokia.gdc.envrpt.check;

import com.nokia.gdc.envrpt.domain.Parameter;
import com.nokia.gdc.envrpt.domain.Report;
import org.apache.commons.lang3.StringUtils;

/**
 *
 * @author Roger Chacón <roger.chacon@nokia.com>
 */
public class CassandraMaxHeapSize extends AbstractCheck {

    public CassandraMaxHeapSize() {
        catName = "JVM Size";
        paramNames = new String[]{"MAX_HEAP_SIZE"};
    }

    @Override
    public String getCommand() {
        return "scripts/CassandraMaxHeapSize.sh";
    }

    @Override
    public Report parseResults(String response) {
        String[] lines = response.split("\n");
        String maxHeapSize = "Not found";
        for (String line : lines) {
            final String trimmed = line.trim();
            final String maxHeapSizeCmd = "MAX_HEAP_SIZE=";
            if (StringUtils.isNotEmpty(trimmed) && trimmed.startsWith(maxHeapSizeCmd)) {
                maxHeapSize = trimmed.substring(maxHeapSizeCmd.length());
            }
        }
        Parameter param = new Parameter(paramNames[0])
                .withStatusInfo()
                .withCurrentValue(maxHeapSize);
        return new Report().withParam(catName, param);
    }

}
