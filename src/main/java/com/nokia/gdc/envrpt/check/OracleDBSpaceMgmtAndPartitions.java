/*
 * Copyright (c) $(year) Nokia Software. All rights reserved.
 * PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nokia.gdc.envrpt.check;

import com.nokia.gdc.envrpt.domain.Report;
import com.nokia.gdc.envrpt.domain.Parameter;
import com.nokia.gdc.envrpt.utils.OracleUtils;

/**
 *
 * @author Roger Chacón <roger.chacon@nokia.com>
 */
public class OracleDBSpaceMgmtAndPartitions extends AbstractCheck {

    public OracleDBSpaceMgmtAndPartitions() {
        catName = "Space Management & Partition Details";
        paramNames = new String[]{"Tables & partitions"};
    }

    @Override
    public String getCommand() {
        return "scripts/OracleDBAdmPwdAndPrivilege.sh " + context.getString("schema");
    }

    @Override
    public Report parseResults(String response) {
        OracleUtils.parseQueryResult(response).forEach(table -> {
            Parameter param = new Parameter(table.get("TABLE_NAME"))
                    .withCurrentValue(table.get("PARTITION_NAME"))
                    .withStatusInfo();
            params.add(param);
        });
        return getDefaultReport();
    }

}
