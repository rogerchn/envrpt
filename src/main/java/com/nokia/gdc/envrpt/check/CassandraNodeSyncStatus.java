/*
 * Copyright (c) $(year) Nokia Software. All rights reserved.
 * PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nokia.gdc.envrpt.check;

import com.nokia.gdc.envrpt.domain.Report;
import com.nokia.gdc.envrpt.domain.Parameter;
import com.nokia.gdc.envrpt.utils.CassandraUtils;

/**
 *
 * @author Roger Chacón <roger.chacon@nokia.com>
 */
public class CassandraNodeSyncStatus extends AbstractCheck {

    public CassandraNodeSyncStatus() {
        catName = "Node Sync Status";
        paramNames = new String[]{"Tables"};
    }

    @Override
    public String getCommand() {
        String cqlUser = context.getString("cqlsh.user");
        String cqlPass = context.getString("cqlsh.pass");
        String cqlHost = context.getString("cqlsh.host");
        String cqlPort = context.getString("cqlsh.port");
        return "scripts/CassandraNodeSyncStatus.sh " + cqlUser + " " + cqlPass + " " + cqlHost + " " + cqlPort;
    }

    @Override
    public Report parseResults(String response) {
        CassandraUtils.parseQueryResult(response).forEach(table -> {
            Parameter param = new Parameter(table.get("keyspace_name") + "." + table.get("table_name"))
                    .withCurrentValue(table.get("last_successful_validation") + "\n" + table.get("last_unsuccessful_validation"))
                    .withStatusInfo();
            params.add(param);
        });
        return getDefaultReport();
    }

}
