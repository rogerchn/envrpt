/*
 * Copyright (c) $(year) Nokia Software. All rights reserved.
 * PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nokia.gdc.envrpt.check;

import com.nokia.gdc.envrpt.domain.Report;
import com.nokia.gdc.envrpt.domain.Parameter;
import com.nokia.gdc.envrpt.utils.OracleUtils;

/**
 *
 * @author Roger Chacón <roger.chacon@nokia.com>
 */
public class OracleDBParam extends AbstractCheck {

    @Override
    public String getCommand() {
        return "scripts/OracleDBParam.sh " + context.getString("paramName");
    }

    @Override
    public Report parseResults(String response) {
        catName = context.getString("categoryName");
        sgmName = context.optString("segmentName");
        paramNames = new String[]{context.getString("paramName")};
        OracleUtils.parseQueryResult(response).forEach(entry -> {
            final String pName = entry.get("NAME");
            final String value = entry.get("VALUE");
            Parameter param = new Parameter(pName)
                    .withCurrentValue(value);
            if (context.has(pName)) {
                final String expected = context.getString(pName);
                param.setExpectedValue(expected);
                if (expected.equals(value)) {
                    param.setStatusGood();
                } else {
                    param.setStatusBad();
                }
            } else {
                param.setStatusInfo();
            }
            params.add(param);
        });
        return getDefaultReport();
    }

}
