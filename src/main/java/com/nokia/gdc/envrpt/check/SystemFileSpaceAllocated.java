/**
 * Copyright (c) $(year) Nokia Software. All rights reserved.
 * PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 */
package com.nokia.gdc.envrpt.check;

import com.nokia.gdc.envrpt.domain.Report;
import com.nokia.gdc.envrpt.domain.Parameter;
import org.apache.commons.lang3.StringUtils;

/**
 *
 * @author Roger Chacón <roger.chacon@nokia.com>
 */
public class SystemFileSpaceAllocated extends AbstractCheck {

    @Override
    public String getCommand() {
        return "scripts/SystemFileSpaceAllocated.sh " + context.getString("path");
    }

    @Override
    public Report parseResults(String response) {
        catName = context.getString("category");
        sgmName = context.getString("path");
        paramNames = new String[]{"File contents"};
        String[] lines = response.split("\n");
        for (String line : lines) {
            if (StringUtils.isNotEmpty(line) && !line.startsWith("du: cannot access")) {
                String folder = line;
                String size = "N/A";
                String[] chunks = line.split("\t");
                if (chunks.length > 0) {
                    size = chunks[0].trim();
                    if (chunks.length > 1) {
                        folder = chunks[1].trim();
                    }
                }
                Parameter param = new Parameter(folder)
                        .withCurrentValue("0".equals(size) ? size + "B" : size)
                        .withStatusInfo();
                params.add(param);
            }
        }
        return getDefaultReport();
    }

}
