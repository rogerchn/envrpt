/*
 * Copyright (c) $(year) Nokia Software. All rights reserved.
 * PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nokia.gdc.envrpt.check;

import com.nokia.gdc.envrpt.domain.Report;
import com.nokia.gdc.envrpt.domain.Parameter;
import com.nokia.gdc.envrpt.utils.CassandraUtils;
import org.apache.commons.lang3.StringUtils;

/**
 *
 * @author Roger Chacón <roger.chacon@nokia.com>
 */
public class CassandraNodetoolVersion extends AbstractCheck {

    public CassandraNodetoolVersion() {
        catName = "Cassandra Version";
        sgmName = "Nodetool";
        paramNames = new String[]{"Node Version"};
    }

    @Override
    public String getCommand() {
        String ntUser = context.getString("nodetool.user");
        String ntPass = context.getString("nodetool.pass");
        return "scripts/CassandraNodetoolVersion.sh " + ntUser + " " + ntPass;
    }

    @Override
    public Report parseResults(String response) {
        if (CassandraUtils.isNodetoolConfigured(response)) {
            String[] lines = response.split("\n");
            for (String line : lines) {
                if (!StringUtils.isEmpty(line)) {
                    String[] chunks = line.split(":");
                    String pName = chunks[0].trim();
                    String pValue = chunks[1].trim();
                    final String expectedValue = context.optString(pName);
                    Parameter param = new Parameter(pName)
                            .withCurrentValue(pValue)
                            .withExpectedValue(expectedValue);
                    if (expectedValue.isEmpty()) {
                        param.setStatusInfo();
                    } else {
                        if (expectedValue.equals(pValue)) {
                            param.setStatusGood();
                        } else {
                            param.setStatusBad();
                        }
                    }
                    params.add(param);
                }
            }
        } else {
            Parameter param = new Parameter(response)
                    .withStatusWarn();
            params.add(param);
        }
        return getDefaultReport();
    }

}
