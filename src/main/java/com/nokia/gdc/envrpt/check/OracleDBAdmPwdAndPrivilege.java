/*
 * Copyright (c) $(year) Nokia Software. All rights reserved.
 * PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nokia.gdc.envrpt.check;

import com.nokia.gdc.envrpt.domain.Report;
import com.nokia.gdc.envrpt.domain.Parameter;
import com.nokia.gdc.envrpt.utils.OracleUtils;

/**
 *
 * @author Roger Chacón <roger.chacon@nokia.com>
 */
public class OracleDBAdmPwdAndPrivilege extends AbstractCheck {

    public OracleDBAdmPwdAndPrivilege() {
        catName = "DBA password and privliege";
        paramNames = new String[]{"DBA roles"};
    }

    @Override
    public String getCommand() {
        return "scripts/OracleDBAdmPwdAndPrivilege.sh";
    }

    @Override
    public Report parseResults(String response) {
        OracleUtils.parseQueryResult(response).forEach(grantee -> {
            StringBuilder privs = new StringBuilder();
            String[] roles = {"GRA", "ADM", "DEL", "DEF", "COM", "INH"};
            boolean firstPriv = true;
            for (String role : roles) {
                if ("YES".equals(grantee.get(role))) {
                    if (firstPriv) {
                        firstPriv = false;
                    } else {
                        privs.append(", ");
                    }
                    privs.append(role);
                }
            }
            Parameter param = new Parameter(grantee.get("GRANTEE"))
                    .withCurrentValue(privs.toString())
                    .withStatusInfo();
            params.add(param);
        });
        return getDefaultReport();
    }

}
