/*
 * Copyright (c) $(year) Nokia Software. All rights reserved.
 * PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nokia.gdc.envrpt.check;

import com.nokia.gdc.envrpt.domain.Report;
import com.nokia.gdc.envrpt.domain.Parameter;

/**
 *
 * @author Roger Chacón <roger.chacon@nokia.com>
 */
public class OracleDBArchiveLogList extends AbstractCheck {

    public OracleDBArchiveLogList() {
        catName = "Backup Policy & Logs";
        sgmName = "Archive Log List";
        paramNames = new String[]{sgmName};
    }

    @Override
    public String getCommand() {
        return "scripts/OracleDBArchiveLogList.sh";
    }

    @Override
    public Report parseResults(String response) {
        String[] lines = response.split("\n");
        for (String line : lines) {
            if (line.length() > 0) {
                int separator = line.trim().lastIndexOf("  ");
                String pName = line.substring(0, separator).trim();
                String value = line.substring(separator).trim();
                Parameter param = new Parameter(pName)
                        .withCurrentValue(value);
                if (context.has(pName)) {
                    String expected = context.getString(pName);
                    param.setExpectedValue(expected);
                    if (expected.equals(value)) {
                        param.setStatusGood();
                    } else {
                        param.setStatusBad();
                    }
                } else {
                    param.setStatusInfo();
                }
                params.add(param);
            }
        }
        return getDefaultReport();
    }

}
