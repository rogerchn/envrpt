/*
 * Copyright (c) $(year) Nokia Software. All rights reserved.
 * PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nokia.gdc.envrpt.check;

import com.nokia.gdc.envrpt.domain.Report;
import com.nokia.gdc.envrpt.domain.Parameter;
import com.nokia.gdc.envrpt.utils.OracleUtils;

/**
 *
 * @author Roger Chacón <roger.chacon@nokia.com>
 */
public class OracleDBTablespaceSize extends AbstractCheck {

    public OracleDBTablespaceSize() {
        catName = "Tablespace Size";
        paramNames = new String[]{"Tablespace"};
    }

    @Override
    public String getCommand() {
        return "scripts/OracleDBTablespaceSize.sh " + context.getString("schema");
    }

    @Override
    public Report parseResults(String response) {
        Integer maxUsage = context.getInt("maxUsage");
        OracleUtils.parseQueryResult(response).forEach(tableSpace -> {
            String pctUsed = tableSpace.get("% USED");
            float used = Float.parseFloat(pctUsed);
            Parameter param = new Parameter(tableSpace.get("TABLESPACE_NAME"))
                    .withCurrentValue(tableSpace.get("USED (MB)") + " MB (" + tableSpace.get("% USED") + "%)")
                    .withExpectedValue(maxUsage.toString());
            if (used <= maxUsage) {
                param.setStatusGood();
            } else {
                param.setStatusBad();
            }
            params.add(param);
        });
        return getDefaultReport();
    }

}
