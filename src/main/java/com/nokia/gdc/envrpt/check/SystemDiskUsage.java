/**
 * Copyright (c) $(year) Nokia Software. All rights reserved.
 * PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 */
package com.nokia.gdc.envrpt.check;

import com.nokia.gdc.envrpt.domain.Report;
import com.nokia.gdc.envrpt.domain.Parameter;

/**
 *
 * @author Roger Chacón <roger.chacon@nokia.com>
 */
public class SystemDiskUsage extends AbstractCheck {

    public SystemDiskUsage() {
        catName = "Disk Usage";
        paramNames = new String[]{"File systems"};
    }

    @Override
    public String getCommand() {
        return "scripts/SystemDiskUsage.sh";
    }

    @Override
    public Report parseResults(String response) {
        String[] lines = response.split("\n");
        boolean firstLine = true;
        for (String line : lines) {
            if (firstLine) { // Discard header (first line)
                firstLine = false;
            } else {
                if (line.length() > 0) { // Discard empty lines
                    line = line.trim().replaceAll(" +", " ");
                    String[] columns = line.split(" ");
                    String usage = columns[4];
                    Float currentUsage = Float.parseFloat(usage.substring(0, usage.indexOf("%")));
                    Float maxUsage = context.getFloat("max-usage");
                    Parameter param = new Parameter(columns[0] + " [" + columns[5] + "]")
                            .withCurrentValue(usage)
                            .withExpectedValue(maxUsage.toString());
                    if (currentUsage >= maxUsage) {
                        param.setStatusBad();
                    } else {
                        param.setStatusGood();
                    }
                    params.add(param);
                }
            }
        }
        return getDefaultReport();
    }

}
