/*
 * Copyright (c) $(year) Nokia Software. All rights reserved.
 * PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nokia.gdc.envrpt.check;

import com.nokia.gdc.envrpt.domain.Report;
import com.nokia.gdc.envrpt.domain.Parameter;
import java.util.Objects;

/**
 *
 * @author Roger Chacón <roger.chacon@nokia.com>
 */
public class OracleDBReplicatedTableCount extends AbstractCheck {

    @Override
    public String getCommand() {
        return "scripts/OracleDBReplicatedTableCount.sh " + context.getString("schemaName") + " " + context.getString("schemaPass") + " " + context.getString("dbLinkName");
    }

    @Override
    public Report parseResults(String response) {
        catName = "Count of Tables Replicated";
        paramNames = new String[]{context.getString("paramName")};
        String[] lines = response.split("\n");
        for (int i = 0; i < lines.length; i++) {
            String line = lines[i];
            if ("===============================".equals(line)) {
                String tableName = lines[++i];
                Integer sourceCount = Integer.parseInt(lines[++i].substring(9));
                Integer targetCount = Integer.parseInt(lines[++i].substring(9));
                Parameter param = new Parameter(tableName)
                        .withCurrentValue(sourceCount.toString())
                        .withExpectedValue(targetCount.toString());
                if (Objects.equals(sourceCount, targetCount)) {
                    param.setStatusGood();
                } else {
                    param.setStatusBad();
                }
                params.add(param);
            }
        }
        return getDefaultReport();
    }

}
