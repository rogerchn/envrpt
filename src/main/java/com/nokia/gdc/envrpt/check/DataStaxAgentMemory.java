/**
 * Copyright (c) $(year) Nokia Software. All rights reserved.
 * PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 */
package com.nokia.gdc.envrpt.check;

import com.nokia.gdc.envrpt.domain.Report;
import com.nokia.gdc.envrpt.domain.Parameter;
import org.apache.commons.lang3.StringUtils;

/**
 *
 * @author Roger Chacón <roger.chacon@nokia.com>
 */
public class DataStaxAgentMemory extends AbstractCheck {

    public DataStaxAgentMemory() {
        catName = "DataStax";
        sgmName = "Agent Memory";
        paramNames = new String[]{sgmName};
    }

    @Override
    public String getCommand() {
        return "scripts/DataStaxAgentMemory.sh";
    }

    @Override
    public Report parseResults(String response) {
        String[] lines = response.split("\n");
        for (String line : lines) {
            if (StringUtils.isNotEmpty(line) && line.startsWith("JVM_OPTS=")) {
                final String value = line.substring(line.indexOf("\""), line.lastIndexOf("\""));
                final String jvmOpt = "Xmx";
                if (value.contains("-" + jvmOpt)) {
                    int pos = value.indexOf("-" + jvmOpt) + 4;
                    String paramValue = value.substring(pos).split(" ")[0];
                    String expectedValue = context.getString(jvmOpt);
                    Parameter param = new Parameter(jvmOpt)
                            .withCurrentValue(paramValue)
                            .withExpectedValue(expectedValue);
                    if (expectedValue.length() > 0) {
                        if (expectedValue.equals(paramValue)) {
                            param.setStatusGood();
                        } else {
                            param.setStatusBad();
                        }
                    } else {
                        param.setStatusInfo();
                    }
                    params.add(param);
                }
            }
        }
        return getDefaultReport();
    }

}
