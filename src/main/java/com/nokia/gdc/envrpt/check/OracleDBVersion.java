/*
 * Copyright (c) $(year) Nokia Software. All rights reserved.
 * PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nokia.gdc.envrpt.check;

import com.nokia.gdc.envrpt.domain.Report;
import com.nokia.gdc.envrpt.domain.Parameter;
import com.nokia.gdc.envrpt.utils.OracleUtils;
import java.util.Map;

/**
 *
 * @author Roger Chacón <roger.chacon@nokia.com>
 */
public class OracleDBVersion extends AbstractCheck {

    public OracleDBVersion() {
        catName = "Product Version";
        paramNames = new String[]{entryName};
    }

    @Override
    public String getCommand() {
        return "scripts/OracleDBVersion.sh";
    }

    @Override
    public Report parseResults(String response) {
        String expVersion = context.getString("version");
        Parameter param = new Parameter(entryName)
                .withExpectedValue(expVersion);
        boolean found = false;
        for (Map<String, String> entry : OracleUtils.parseQueryResult(response)) {
            final String banner = entry.get("BANNER");
            if (banner.startsWith(entryName)) {
                String curVersion = banner.substring(entryName.length() + 1);
                param.setCurrentValue(curVersion);
                if (curVersion.equals(expVersion)) {
                    param.setStatusGood();
                } else {
                    param.setStatusBad();
                }
                found = true;
            }
        }
        if (!found) {
            param.setStatusWarn();
            param.setCurrentValue("N/A");
        }
        params.add(param);
        return getDefaultReport();
    }

    private final String entryName = "Oracle Database";

}
