/**
 * Copyright (c) $(year) Nokia Software. All rights reserved.
 * PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 */
package com.nokia.gdc.envrpt.check;

import com.nokia.gdc.envrpt.domain.Report;
import com.nokia.gdc.envrpt.domain.Parameter;
import org.apache.commons.lang3.StringUtils;

/**
 *
 * @author Roger Chacón <roger.chacon@nokia.com>
 */
public class SystemCPUUsage extends AbstractCheck {

    public SystemCPUUsage() {
        catName = "CPU Usage";
        paramNames = new String[]{"Processes"};
    }

    @Override
    public String getCommand() {
        return "scripts/SystemCPUUsage.sh";
    }

    @Override
    public Report parseResults(String response) {
        String[] lines = response.split("\n");
        for (String line : lines) {
            if (StringUtils.isNotEmpty(line) && !line.startsWith("%CPU")) {
                String trimmedLine = line.trim().replaceAll("\\s+", " ");
                String[] chunks = trimmedLine.split(" ");
                String usage = chunks[0];
                String command = chunks[3];
                StringBuilder args = new StringBuilder();
                int i = 4;
                while (i < chunks.length) {
                    args.append(chunks[i++]).append(" ");
                }
                Parameter param = new Parameter(command)
                        .withCurrentValue(usage + "_%")
                        .withStatusInfo();
                params.add(param);
            }
        }
        return getDefaultReport();
    }

}
