/**
 * Copyright (c) $(year) Nokia Software. All rights reserved.
 * PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 */
package com.nokia.gdc.envrpt.check;

import com.nokia.gdc.envrpt.domain.Report;
import com.nokia.gdc.envrpt.domain.Parameter;
import org.apache.commons.lang3.StringUtils;

/**
 *
 * @author Roger Chacón <roger.chacon@nokia.com>
 */
public class CassandraMetricsRollups extends AbstractCheck {

    public CassandraMetricsRollups() {
        catName = "Cassandra Metrics";
        sgmName = "Rollups";
    }

    @Override
    public String getCommand() {
        return "scripts/CassandraMetricsRollups.sh";
    }

    @Override
    public Report parseResults(String response) {
        String[] lines = response.split("\n");
        for (String line : lines) {
            if (StringUtils.isNotEmpty(line) && line.contains("=")) {
                String[] chunks = line.split("=");
                String ttlName = chunks[0].trim();
                String ttlValue = chunks[1].trim();
                String expectedValue = context.optString(ttlName);
                Parameter param = new Parameter(ttlName)
                        .withCurrentValue(ttlValue)
                        .withExpectedValue(expectedValue);
                if (expectedValue.isEmpty()) {
                    param.setStatusInfo();
                } else {
                    if (expectedValue.equals(ttlValue)) {
                        param.setStatusGood();
                    } else {
                        param.setStatusBad();
                    }
                }
                params.add(param);
            }
        }
        return getDefaultReport();
    }

}
