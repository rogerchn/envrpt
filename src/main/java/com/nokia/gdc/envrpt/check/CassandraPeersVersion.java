/*
 * Copyright (c) $(year) Nokia Software. All rights reserved.
 * PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nokia.gdc.envrpt.check;

import com.nokia.gdc.envrpt.domain.Report;
import com.nokia.gdc.envrpt.domain.Parameter;
import com.nokia.gdc.envrpt.utils.CassandraUtils;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Roger Chacón <roger.chacon@nokia.com>
 */
public class CassandraPeersVersion extends AbstractCheck {

    public CassandraPeersVersion() {
        catName = "Cassandra Version";
        sgmName = "Peers";
        paramNames = new String[]{"Peer"};
    }

    @Override
    public String getCommand() {
        String cqlUser = context.getString("cqlsh.user");
        String cqlPass = context.getString("cqlsh.pass");
        String cqlHost = context.getString("cqlsh.host");
        String cqlPort = context.getString("cqlsh.port");
        return "scripts/CassandraPeersVersion.sh " + cqlUser + " " + cqlPass + " " + cqlHost + " " + cqlPort;
    }

    @Override
    public Report parseResults(String response) {
        List<Map<String, String>> entries = CassandraUtils.parseQueryResult(response);
        if (entries.isEmpty()) {
            Parameter param = new Parameter("No peers found.")
                    .withStatusWarn();
            params.add(param);
        } else {
            entries.forEach(entry -> {
                String expectedVersion = context.optString("version");
                String releaseVersion = entry.get("release_version");
                Parameter param = new Parameter(entry.get("peer"))
                        .withCurrentValue(releaseVersion)
                        .withExpectedValue(expectedVersion);
                if (expectedVersion.length() > 0) {
                    if (expectedVersion.equals(releaseVersion)) {
                        param.setStatusGood();
                    } else {
                        param.setStatusBad();
                    }
                } else {
                    param.setStatusInfo();
                }
                params.add(param);
            });
        }
        return getDefaultReport();
    }

}
