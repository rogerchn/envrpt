/**
 * Copyright (c) $(year) Nokia Software. All rights reserved.
 * PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 */
package com.nokia.gdc.envrpt.check;

import com.nokia.gdc.envrpt.domain.Report;
import com.nokia.gdc.envrpt.domain.Parameter;
import org.apache.commons.lang3.StringUtils;

/**
 *
 * @author Roger Chacón <roger.chacon@nokia.com>
 */
public class CassandraMetricsHeapSize extends AbstractCheck {

    public CassandraMetricsHeapSize() {
        this.catName = "Cassandra Metrics";
        this.sgmName = "Heap Size";
        this.paramNames = new String[]{
            "MAX_HEAP_SIZE",
            "HEAP_NEWSIZE"
        };
    }

    @Override
    public String getCommand() {
        return "scripts/CassandraMetricsHeapSize.sh";
    }

    @Override
    public Report parseResults(String response) {
        String[] lines = response.split("\n");
        for (String line : lines) {
            if (StringUtils.isNotEmpty(line) && !line.startsWith("#")) {
                line = line.trim();
                for (String paramName : paramNames) {
                    if (line.contains(paramName + "=")) {
                        String[] chunks = line.split("=");
                        String optName = chunks[0].trim();
                        String optValue = chunks[1].trim();
                        if (optName.equals(paramName)) {
                            Parameter param = new Parameter(optName).withCurrentValue(optValue);
                            String expectedValue = "\"" + context.optString(paramName) + "\"";
                            param.setExpectedValue(expectedValue);
                            if (expectedValue.isEmpty()) {
                                param.setStatusInfo();
                            } else {
                                if (expectedValue.equals(optValue)) {
                                    param.setStatusGood();
                                } else {
                                    param.setStatusBad();
                                }
                            }
                            params.add(param);
                        }
                    }
                }
            }
        }
        return getDefaultReport();
    }

}
