/*
 * Copyright (c) $(year) Nokia Software. All rights reserved.
 * PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nokia.gdc.envrpt.check;

import com.nokia.gdc.envrpt.domain.Report;
import com.nokia.gdc.envrpt.domain.Parameter;
import com.nokia.gdc.envrpt.utils.OracleUtils;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Roger Chacón <roger.chacon@nokia.com>
 */
public class OracleDBActiveSessions extends AbstractCheck {

    public OracleDBActiveSessions() {
        catName = "Active Sessions";
        paramNames = new String[]{paramName};
    }

    @Override
    public String getCommand() {
        return "scripts/OracleDBActiveSessions.sh";
    }

    @Override
    public Report parseResults(String response) {
        Integer maxCount = context.getInt("maxCount");
        List<Map<String, String>> results = OracleUtils.parseQueryResult(response);
        String value = "N/A";
        int sessionCount = Integer.MIN_VALUE;
        boolean recordFound = false;
        if (results.size() > 0) {
            value = results.get(0).get("VSCOUNT");
            sessionCount = Integer.parseInt(value);
            recordFound = true;
        }
        Parameter param = new Parameter(paramName)
                .withCurrentValue(value)
                .withExpectedValue(maxCount.toString());
        if (recordFound && sessionCount <= maxCount) {
            param.setStatusGood();
        } else {
            param.setStatusBad();
        }
        params.add(param);
        return getDefaultReport();
    }

    private final String paramName = "Number of active sessions";

}
