/*
 * Copyright (c) $(year) Nokia Software. All rights reserved.
 * PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nokia.gdc.envrpt.check;

import com.nokia.gdc.envrpt.domain.Report;
import com.nokia.gdc.envrpt.domain.Parameter;
import com.nokia.gdc.envrpt.utils.CassandraUtils;
import org.json.JSONObject;

/**
 *
 * @author Roger Chacón <roger.chacon@nokia.com>
 */
public class CassandraReplicationFactor extends AbstractCheck {

    public CassandraReplicationFactor() {
        catName = "Replication Factor";
        paramNames = new String[]{"Commands"};
    }

    @Override
    public String getCommand() {
        String cqlUser = context.getString("cqlsh.user");
        String cqlPass = context.getString("cqlsh.pass");
        String cqlHost = context.getString("cqlsh.host");
        String cqlPort = context.getString("cqlsh.port");
        String keyspace = context.getString("keyspace");
        return "scripts/CassandraReplicationFactor.sh " + cqlUser + " " + cqlPass + " " + cqlHost + " " + cqlPort + " " + keyspace;
    }

    @Override
    public Report parseResults(String response) {
        CassandraUtils.parseQueryResult(response).forEach(entry -> {
            JSONObject rpl = new JSONObject(entry.get("replication").replace("'", "\""));
            String paramValue = "Replication Factor: " + rpl.getString("replication_factor") + " (" + rpl.getString("class") + ")";
            Parameter param = new Parameter(entry.get("keyspace_name"))
                    .withCurrentValue(paramValue)
                    .withStatusInfo();
            params.add(param);
        });
        return getDefaultReport();
    }

}
