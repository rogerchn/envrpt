/*
 * Copyright (c) $(year) Nokia Software. All rights reserved.
 * PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nokia.gdc.envrpt.check;

import com.nokia.gdc.envrpt.domain.Report;
import com.nokia.gdc.envrpt.domain.Parameter;

/**
 *
 * @author Roger Chacón <roger.chacon@nokia.com>
 */
public class OracleDBRmanConfigParams extends AbstractCheck {

    public OracleDBRmanConfigParams() {
        catName = "Backup Policy & Logs";
        sgmName = "RMAN Configuration";
        paramNames = new String[]{sgmName};
    }

    @Override
    public String getCommand() {
        return "scripts/OracleDBRmanConfigParams.sh";
    }

    @Override
    public Report parseResults(String response) {
        final String prefix = "CONFIGURE ";
        final String sep1 = " TO ";
        final String sep2 = " ";
        for (String line : response.split("\n")) {
            if (line.startsWith(prefix)) {
                String config = line.substring(prefix.length(), line.length() - 1).trim();
                String configName;
                String configValue;
                if (config.contains(sep1)) {
                    String[] chunks = config.split(sep1);
                    configName = chunks[0].trim();
                    configValue = chunks[1].trim();
                } else {
                    int pos = config.lastIndexOf(sep2);
                    configName = config.substring(0, pos);
                    configValue = config.substring(pos + 1);
                }
                Parameter param = new Parameter(configName)
                        .withCurrentValue(configValue);
                if (context.has(configName)) {
                    String expected = context.getString(configName);
                    param.setExpectedValue(expected);
                    if (configValue.equals(expected)) {
                        param.setStatusGood();
                    } else {
                        param.setStatusBad();
                    }
                } else {
                    param.setStatusInfo();
                }
                params.add(param);
            }
        }
        return getDefaultReport();
    }

}
