/**
 * Copyright (c) $(year) Nokia Software. All rights reserved.
 * PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 */
package com.nokia.gdc.envrpt.check;

import com.nokia.gdc.envrpt.domain.Report;
import com.nokia.gdc.envrpt.domain.Parameter;

/**
 *
 * @author Roger Chacón <roger.chacon@nokia.com>
 */
public class WinVersion extends AbstractCheck {

    public WinVersion() {
        catName = "Win Version";
    }

    @Override
    public String getCommand() {
        return "systeminfo";
    }

    @Override
    public Report parseResults(String response) {
        String[] lines = response.split("\n");
        for (String line : lines) {
            if (line.startsWith("OS ")) {
                String[] chunks = line.split(":");
                Parameter entry = new Parameter(chunks[0].trim())
                        .withCurrentValue(chunks[1].trim())
                        .withStatusGood();
                params.add(entry);
            }
        }
        return getDefaultReport();
    }

}
