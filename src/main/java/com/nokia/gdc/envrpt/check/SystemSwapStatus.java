/**
 * Copyright (c) $(year) Nokia Software. All rights reserved.
 * PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 */
package com.nokia.gdc.envrpt.check;

import com.nokia.gdc.envrpt.domain.Report;
import com.nokia.gdc.envrpt.domain.Category;
import com.nokia.gdc.envrpt.domain.Parameter;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.lang3.StringUtils;

/**
 *
 * @author Roger Chacón <roger.chacon@nokia.com>
 */
public class SystemSwapStatus extends AbstractCheck {

    public SystemSwapStatus() {
        catName = "System SWAP Memory";
        sgmName = "Total System Memory";
        paramNames = new String[]{"Status"};
    }

    @Override
    public String getCommand() {
        return "scripts/SystemSwapStatus.sh";
    }

    @Override
    public Report parseResults(String response) {
        String[] lines = response.split("\n");
        String status = "Disabled";
        for (int i = 1; i < lines.length; i++) {
            String line = lines[i];
            if (StringUtils.isNotEmpty(line)) {
                String trimmedLine = line.trim().replaceAll("\\s+", " ");
                String[] chunks = trimmedLine.split(" ");
                String memType = chunks[0].split(":")[0];
                Integer totalMem = Integer.parseInt(chunks[1]);
                if ("Swap".equalsIgnoreCase(memType) && totalMem > 0) {
                    status = "Enabled";
                }
                Parameter param = new Parameter(memType)
                        .withCurrentValue(totalMem.toString())
                        .withStatusInfo();
                params.add(param);
            }
        }
        Report report = getDefaultReport();
        Category category = report.getCategory(catName);
        String expectedStatus = context.getString("status");
        Parameter param = new Parameter(paramNames[0])
                .withCurrentValue(status)
                .withExpectedValue(expectedStatus);
        if (status.equalsIgnoreCase(expectedStatus)) {
            param.setStatusGood();
        } else {
            param.setStatusBad();
        }
        List<Parameter> catParams = new ArrayList<>();
        catParams.add(param);
        category.setParams(catParams);
        return report;
    }

}
