/*
 * Copyright (c) $(year) Nokia Software. All rights reserved.
 * PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nokia.gdc.envrpt.check;

import com.nokia.gdc.envrpt.domain.Report;
import com.nokia.gdc.envrpt.domain.Parameter;

/**
 *
 * @author Roger Chacón <roger.chacon@nokia.com>
 */
public class OracleGGVersion extends AbstractCheck {

    public OracleGGVersion() {
        catName = "Product Version";
        paramNames = new String[]{entryName};
    }

    @Override
    public String getCommand() {
        return "scripts/OracleGoldenGateVersion.sh";
    }

    @Override
    public Report parseResults(String response) {
        String expVersion = context.getString("version");

        String[] lines = response.split("\n");
        boolean isFirstLine = true;
        for (String line : lines) {
            if (isFirstLine) {
                // Discard the first line
                isFirstLine = false;
            } else {
                // Second line will provide the version in the format:
                // Version xx.x.x.x xxxxx
                String curVersion = line.substring(8);
                Parameter param = new Parameter(entryName)
                        .withCurrentValue(curVersion)
                        .withExpectedValue(expVersion);
                if (curVersion.equals(expVersion)) {
                    param.setStatusGood();
                } else {
                    param.setStatusBad();
                }
                params.add(param);
                break;
            }
        }
        return getDefaultReport();
    }

    private final String entryName = "GoldenGate Command Interpreter";

}
