/**
 * Copyright (c) $(year) Nokia Software. All rights reserved.
 * PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 */
package com.nokia.gdc.envrpt.check;

import com.nokia.gdc.envrpt.domain.Report;
import org.json.JSONObject;

/**
 * This interface is designed to provide a common protocol for objects that
 * intend to check system or server parameters.
 *
 * @author Roger Chacón <roger.chacon@nokia.com>
 */
public interface Checkable {

    /**
     * Sets the context for executing and evaluating this operation.
     *
     * @param context A JSONObject containing the data needed for the operation.
     */
    public void setContext(JSONObject context);

    /**
     * Provides a command to be executed in the system to perform this check.
     *
     * @return A String containing the command to be executed in the system.
     */
    public String getCommand();

    /**
     * Indicates whether errors must be accepted as part of a successful result.
     *
     * @return true if errors must be ignored, false otherwise.
     */
    public boolean isIgnoreErrors();

    /**
     * Converts to JSON notation the output obtained from executing the provided
     * command.
     *
     * @param response A text containing the raw response from the system.
     * @return A Report containing the parsed data.
     */
    public Report parseResults(String response);

    /**
     * Returns a default output to be used in case of error
     *
     * @return A Report containing the default error data.
     */
    public Report getErrorResult();

}
