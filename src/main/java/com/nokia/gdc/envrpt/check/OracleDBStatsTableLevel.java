/*
 * Copyright (c) $(year) Nokia Software. All rights reserved.
 * PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nokia.gdc.envrpt.check;

import com.nokia.gdc.envrpt.domain.Report;
import com.nokia.gdc.envrpt.utils.DateUtils;
import com.nokia.gdc.envrpt.domain.Parameter;
import com.nokia.gdc.envrpt.utils.OracleUtils;
import java.util.Date;

/**
 *
 * @author Roger Chacón <roger.chacon@nokia.com>
 */
public class OracleDBStatsTableLevel extends AbstractCheck {

    public OracleDBStatsTableLevel() {
        catName = "Check Stats - Table Level";
        paramNames = new String[]{"Table Level"};
    }

    @Override
    public String getCommand() {
        return "scripts/OracleDBStatsTableLevel.sh " + context.getString("schema") + " " + context.getString("passwd");
    }

    @Override
    public Report parseResults(String response) {
        final String ctxLastAnalyzed = context.getString("lastAnalyzed");
        final String dtPattern = "dd/MM/yyyy HH:mm:ss";
        final Date minLastAnalyzed = DateUtils.parseString(ctxLastAnalyzed, dtPattern);
        OracleUtils.parseQueryResult(response).forEach(entry -> {
            final String tableName = entry.get("TABLE_NAME");
            final String lastAnalyzed = entry.get("LAST_ANALIZED");
            final Date lastAnalyzedDt = DateUtils.parseString(lastAnalyzed, dtPattern);
            Parameter param = new Parameter(tableName)
                    .withCurrentValue(lastAnalyzed)
                    .withExpectedValue(ctxLastAnalyzed);
            if (lastAnalyzedDt.before(minLastAnalyzed)) {
                param.setStatusBad();
            } else {
                param.setStatusGood();
            }
            params.add(param);
        });
        return getDefaultReport();
    }

}
