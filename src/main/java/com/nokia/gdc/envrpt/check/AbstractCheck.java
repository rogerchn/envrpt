/**
 * Copyright (c) $(year) Nokia Software. All rights reserved.
 * PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 */
package com.nokia.gdc.envrpt.check;

import com.nokia.gdc.envrpt.domain.Report;
import org.json.JSONObject;
import com.nokia.gdc.envrpt.domain.Parameter;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Roger Chacón <roger.chacon@nokia.com>
 */
public abstract class AbstractCheck implements Checkable {

    @Override
    public void setContext(JSONObject context) {
        this.context = context;
    }

    @Override
    public abstract String getCommand();

    @Override
    public abstract Report parseResults(String response);

    @Override
    public Report getErrorResult() {
        for (String paramName : paramNames) {
            Parameter param = new Parameter(paramName)
                    .withStatus(Parameter.STATUS_WARN)
                    .withCurrentValue("N/A");
            params.add(param);
        }
        Report report = new Report();
        if (sgmName.isEmpty()) {
            report.addParams(catName, params);
        } else {
            report.addParams(catName, sgmName, params);
        }
        return report;
    }

    @Override
    public boolean isIgnoreErrors() {
        return ignoreErrors;
    }

    protected JSONObject context;
    protected String catName = "Default Check";
    protected String sgmName = "";
    protected String[] paramNames = {"Unknown parameter"};
    protected List<Parameter> params = new ArrayList<>();
    protected boolean ignoreErrors;

    protected Report getDefaultReport() {
        return new Report().withParams(catName, sgmName, params);
    }

}
