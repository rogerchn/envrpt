/**
 * Copyright (c) $(year) Nokia Software. All rights reserved.
 * PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 */
package com.nokia.gdc.envrpt.check;

import com.nokia.gdc.envrpt.domain.Report;
import com.nokia.gdc.envrpt.domain.Parameter;

/**
 *
 * @author Roger Chacón <roger.chacon@nokia.com>
 */
public class SystemRAMMemory extends AbstractCheck {

    @Override
    public String getCommand() {
        return "scripts/SystemRAMMemory.sh";
    }

    @Override
    public Report parseResults(String response) {
        catName = "RAM memory";
        String[] lines = response.split("\n");
        for (String line : lines) {
            if (line.length() > 0) { // Discard empty lines
                String[] columns = line.split(":");
                String paramName = columns[0].trim();
                if (paramName.equals("MemAvailable")) {
                    String paramValue = columns[1].trim();
                    Integer memAvailable = Integer.parseInt(paramValue.split(" ")[0].trim());
                    Integer minAvailable = context.getInt("min-available");
                    Parameter param = new Parameter(paramName)
                            .withCurrentValue(paramValue)
                            .withExpectedValue(minAvailable.toString());
                    if (memAvailable < minAvailable) {
                        param.setStatusBad();
                    } else {
                        param.setStatusGood();
                    }
                    params.add(param);
                }
            }
        }
        return getDefaultReport();
    }

}
