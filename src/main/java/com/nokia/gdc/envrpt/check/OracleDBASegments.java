/*
 * Copyright (c) $(year) Nokia Software. All rights reserved.
 * PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.nokia.gdc.envrpt.check;

import com.nokia.gdc.envrpt.domain.Report;
import com.nokia.gdc.envrpt.domain.Parameter;
import com.nokia.gdc.envrpt.utils.OracleUtils;

/**
 *
 * @author Roger Chacón <roger.chacon@nokia.com>
 */
public class OracleDBASegments extends AbstractCheck {

    public OracleDBASegments() {
        catName = "DBA Segments";
        paramNames = new String[]{"DBA roles"};
    }

    @Override
    public String getCommand() {
        return "scripts/OracleDBASegments.sh";
    }

    @Override
    public Report parseResults(String response) {
        OracleUtils.parseQueryResult(response).forEach(segment -> {
            Parameter param = new Parameter(segment.get("SEGMENT_NAME"))
                    .withCurrentValue(segment.get("SIZE in GB") + " GB")
                    .withStatusInfo();
            params.add(param);
        });
        return getDefaultReport();
    }

}
